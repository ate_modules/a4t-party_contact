Party Contact
#############

.. warning::
   If you install this module on database that contains data please read the 
   paragraph 'Installation Supplement' of the INSTALL file

We believe that the management of the party's addresses proposed Tryton is not
complete and therefore we propose the following changes in this module.

  * Adding of the possibility to define a default address on party (this 
    address does not need to show the name field)
  * Adding the association between contact mechanism and addresses (on an idea 
    of Ziczacmedia)
  * Help define the address of a contact is identical to the default address
    of the party to avoid having to re-enter informations.