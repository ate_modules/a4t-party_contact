# -*- coding: utf-8 -*-
# This file is part of Party Contact Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .address import *
from .party import *
from .contact_mechanism import *

def register():
    Pool.register(
        Address,
        Party,
        ContactMechanism,
        module='party_contact', type_='model')