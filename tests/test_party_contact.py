#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Party Contact Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import test_view, test_depends, \
    POOL, DB_NAME, USER, CONTEXT
from trytond.transaction import Transaction

class PartyContactTestCase(unittest.TestCase):
    'Test Party Contact module'

    def setUp(self):
        # Install the module
        trytond.tests.test_tryton.install_module('party_contact')

    def test0005views(self):
        'Test views'
        test_view('party_contact')

    def test0006depends(self):
        'Test depends'
        test_depends()

def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        PartyContactTestCase))
    # Add any other tests from this module
    return suite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(suite())