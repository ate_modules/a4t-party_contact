# -*- coding: utf-8 -*-
# This file is part of Party Contact Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from trytond.model import Model, ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.pool import Pool
from .address import XFIELDS

__all__ = ['Party',]

STATES = {
    'readonly': ~Eval('active', True),
}
DEPENDS = ['active']

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class Party(ModelSQL, ModelView):
    "Party"
    __name__ = 'party.party'

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        if not cls.addresses.on_change:
            cls.addresses.on_change = []
        if 'addresses' not in cls.addresses.on_change:
            cls.addresses.on_change.append('addresses')

    def on_change_addresses(self):
        Address = Pool().get('party.address')
        res = {}
        tmp = {}
        main_addrs = [self.addresses.index(x) for x in self.addresses if x.main]
        # For old addresses (before installation of the module), if there are
        #already a main address
        if len(main_addrs) > 1:
            # the new addresse doesn't is the main also
            res = {'addresses': {
                'update': [{'id': self.addresses[-1].id ,'main': False}]
            }}
        # If
        if self.addresses[-1].same_as_main:
            if main_addrs:
                res.setdefault('addresses', {})
                res['addresses'].setdefault('update', [])
                fields = fields_updates = list(
                    set(self.addresses[main_addrs[0]]._values.keys()) & \
                    set(x for x in Address._fields.keys() if x not in XFIELDS))
                for field in fields:
                    value = getattr(self.addresses[main_addrs[0]], field)
                    if isinstance(value, Model):
                        tmp[field] = value.id
                    else:
                        tmp[field] = value
                if tmp:
                    tmp['id'] = self.addresses[-1].id
                    res['addresses']['update'].append(tmp)
        _logger.debug('[on_change_addresses] %s' % (res))
        return res