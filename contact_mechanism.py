# -*- coding: utf-8 -*-
# This file is part of Party Contact Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval

__all__ = ['ContactMechanism']
__metaclass__ = PoolMeta

STATES = {
    'readonly': ~Eval('active'),
    }

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)

class ContactMechanism:
    "Contact Mechanism"
    __name__ = 'party.contact_mechanism'

    address = fields.Many2One('party.address', 'Address',
        required=True,
        domain=[('party', '=', Eval('party'))],
        ondelete='CASCADE', states=STATES, select=True,
        depends=['active', 'party'])
    name = fields.Function(fields.Char('Name'), 'get_name')

    @classmethod
    def get_name(self, contacts, name):
        names = {}
        for contact in contacts:
            names[contact.id] = contact.address and \
                contact.address.name or False
        return names

    @classmethod
    def create(cls, vlist):
        for values in vlist:
            if values.get('address'):
                address = Pool().get('party.address')(values.get('address'))
                values['party'] = address.party
        return super(ContactMechanism, cls).create(vlist)