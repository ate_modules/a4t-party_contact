# -*- coding: utf-8 -*-
# This file is part of Party Contact Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from trytond.model import Model, ModelView, ModelSQL, fields
from trytond.pyson import Eval, If, Bool, Id
from trytond.transaction import Transaction
from trytond import backend
from trytond.pool import Pool

__all__ = ['Address']

STATES = {
    'readonly': ~Eval('active'),
    }
DEPENDS = ['active']
XFIELDS = (
    'id', 'create_uid', 'create_date', 'write_uid', 'write_date', 'party',
    'active', 'sequence', 'full_address', 'rec_name', 'name', 'main',
    'same_as_main', 'delivery', 'invoice', 'main_readonly',
    )

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class Address(ModelSQL, ModelView):
    "Address"
    __name__ = 'party.address'

    main = fields.Boolean('Main address',
        states={
            'readonly': Eval('main_readonly', True),
            },
        depends=DEPENDS + ['main_readonly'],
        on_change_with=['party',],
        help="Define if this address is the main address or not (the main "
            "address don't need to have name).")
    same_as_main = fields.Boolean('Same as main',
        depends=DEPENDS + ['main'],
        #on_change=['same_as_main', 'party',],
        states={
            'invisible': Eval('main', False),
            },
        help="This address is the same as the main address."
        )
    main_readonly = fields.Function(fields.Boolean('Main Readonly'),
        'get_main_readonly')

    @classmethod
    def __setup__(cls):
        super(Address, cls).__setup__()
        # Add states attribut on the field name, if it not already defined ...
        if not cls.name.states:
            cls.name.states = {}
        bck = cls.name.states.get('invisible', False)
        new = (Bool(Eval('main')))
        cls.name.states.update({'invisible': new if not bck else bck | new})
        #
        fields_names = list(x for x in cls._fields.keys() if x not in XFIELDS)
        for field_name in fields_names:
            field = getattr(cls, field_name)
            if not field.states:
                field.states = {}
            bck = field.states.get('readonly', False)
            new = (Bool(Eval('same_as_main')))
            field.states.update({'readonly': new if not bck else bck | new})
        # Add new fields to 'on_change' of party
        if not cls.party.on_change:
            cls.party.on_change = []
        for field in ('same_as_main', 'party'):
            if field not in cls.party.on_change:
                cls.party.on_change.append(field)
        # Some error messages
        cls._error_messages.update({
            'check_main': ("A main address is already defined on the party: "
                "%(party)s"),
        })
        # ... In first the main address
        cls._order.insert(0, ('main', 'DESC'))

    @staticmethod
    def default_main():
        return True

    @staticmethod
    def default_same_as_main():
        return False

    @staticmethod
    def default_main_readonly():
        return True

    def on_change_with_main(self):
        address = self.search([('party', '=', self.party),('main', '=', True)])

    def on_change_party(self):
        res = {}
        _logger.debug('[on_change_same_as_main] %s' % (self.addresses))
        if self.same_as_main and self.party:
            addresses = self.search([
                ('party', '=', self.party),
                ('main', '=', True),
                ])
            if addresses:
                address, = addresses
                fields_names = list(x for x in self._fields.keys()
                    if x not in XFIELDS)
                # For all fields not in list XFIELDS ...
                for field_name in fields_names:
                    value = getattr(address, field_name)
                    if isinstance(value, Model):
                        res[field_name] = value.id
                    else:
                        res[field_name] = value
        return res

    def get_main_readonly(self, name):
        if self.party and \
                len([x.id for x in self.party.addresses if x.main]) < 1:
            _logger.debug("[get_main_readonly] No main address")
            return False
        return True

    @classmethod
    def validate(cls, addresses):
        super(Address, cls).validate(addresses)
        for address in addresses:
            address.check_main()

    def check_main(self):
        'Check if there are not two main addresses'
        count = self.search_count([
            ('party', '=', self.party),
            ('main', '=', True)
        ])
        if count >= 2:
            self.raise_user_error('check_main', {'party': self.party.name})

    @classmethod
    def write(cls, addresses, vals):
        vals2 = {}
        addrs2 = []
        fields_updates = list(set(vals.keys()) \
            & set(x for x in cls._fields.keys() if x not in XFIELDS))
        _logger.debug('[write] Fields:%s' % (fields_updates,))
        for address in addresses:
            # if the modified address is the primary address and if one of
            # modified fields in the list of synchronized fields.
            if address.main and fields_updates:
                same_addr = cls.search([
                    ('same_as_main', '=', True),
                    ('party', '=', address.party)
                ])
                _logger.debug('[write] Idem:%s' % (same_addr,))
                if same_addr:
                    for field_name in fields_updates:
                        vals2[field_name] = vals.get(field_name)
                    addrs2.extend(same_addr)
        _logger.debug('[write] 2 %s:%s' % (addrs2,vals2,))
        _logger.debug('[write] 1 %s:%s' % (addresses,vals,))
        if addrs2 and vals2:
            super(Address, cls).write(addrs2, vals2)
        super(Address, cls).write(addresses, vals)