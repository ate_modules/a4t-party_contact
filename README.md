Party Contact
=============

Adiczion's Tryton Module: party_contact
---------------------------------------

_If you install this module on database that contains data please read the 
paragraph 'Installation Supplement' of the INSTALL file_

Modifies the use of parties and addresses to add :

  * Adding of the possibility to define a default address on party (this 
    address does not need to show the name field)
  * Adding the association between contact mechanism and addresses (on an idea 
    of Zikzakmedia)
  * Help define the address of a contact is identical to the default address
    of the party to avoid having to re-enter informations.

Installing
----------

See INSTALL

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

  Adiczion
  --------
  website: http://www.adiczion.com/
  email: atm@adiczion.net

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  http://bugs.tryton.org/
  http://groups.tryton.org/
  http://wiki.tryton.org/
  irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

Additional information
----------------------

For more information please visit the Tryton web site:

  http://www.tryton.org/
